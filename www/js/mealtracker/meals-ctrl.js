var app = angular.module('mealtrack.controllers.meals', []);


app.controller('MealListCtrl', function ($scope, MealService) {
    
    
    $scope.meals = MealService.load();
    

});


app.controller('MealCreateCtrl', function ($scope, $state, $ionicPopup,  $ionicLoading, MealService, $cordovaCamera) {
    
    
    $scope.resetFormData = function () {
         $scope.formData = {
         'title' : '',
         'category' : '',
         'picture' : null
        };
    }
    
    $scope.resetFormData();
    
    
    
    $scope.trackMeal = function (form) {
   
   /*
          $ionicPopup.alert( 
           {title: $scope.formData.title,
				subTitle: $scope.formData.category
            }
            );
            return;
            */
            $ionicLoading.show();
            MealService.track($scope.formData).then(function () {
                
                $ionicLoading.hide();
                $scope.resetFormData();
                $state.go("tab.meals");
                
                
            });
            
            

    }
    
    	
$scope.addPicture = function () {
		var options = {
			quality: 50,
			destinationType: Camera.DestinationType.DATA_URL,
			sourceType: Camera.PictureSourceType.CAMERA, // CAMERA
			allowEdit: true,
			encodingType: Camera.EncodingType.JPEG,
			targetWidth: 480,
			popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: false
		};

		$cordovaCamera.getPicture(options).then(function (imageData) {
            
			$scope.formData.picture = imageData;
            
		}, function (err) {
			console.error(err);
			$ionicPopup.alert({
				title:'Error getting picture',
				subTitle: 'We had a problem trying to get that picture, please try again'
			});
		});
	};
  
})

