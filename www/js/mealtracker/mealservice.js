var app = angular.module('mealtrack.services.meals',['firebase']);

app.service("MealService", function ($firebaseArray, $q , FIREBASE_API) {
    
    
    var meals = [];
    
    
    
    
    var self = {
        
      'track' : function (data) {
        
        
          var d = $q.defer();
          var ref = new Firebase( FIREBASE_API.url + "meals");
          var syncArray = $firebaseArray(ref);
          
          syncArray.$add({title : data.title, category : data.category, image : data.picture}).then(function () {
            
            d.resolve();
            
          });
        
        
        return d.promise;
        
      },

        
        'load' : function () {
            
            var ref = new Firebase(FIREBASE_API.url + "meals");
            var meals  = $firebaseArray(ref);
           
            
            return meals;
            
            
            
        }
        
        
    }
    
    
    
    
    
    return self;
    
    
    
    
    
});
